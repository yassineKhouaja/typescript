import { IPlayer } from "./interfaces";

export class Player implements IPlayer {
  _name: string;
  _shape: "X" | "O";

  constructor(name: string, shape: "X" | "O") {
    if (!Player.isName(name)) throw new Error("Invalid Player name");
    if (!Player.isShape(shape)) throw new Error("Invalid Player shape");
    this._name = name;
    this._shape = shape;
  }

  static isName(name: string) {
    return Boolean(/^[a-zA-z\s]{2,}$/.test(name));
  }

  static isShape(shape: "X" | "O") {
    return Boolean(["X", "O"].includes(shape));
  }

  get name() {
    return this._name;
  }

  set name(newName) {
    this._name = newName;
  }

  get shape() {
    return this._shape;
  }

  set shape(newShape) {
    this.shape = newShape;
  }
}
