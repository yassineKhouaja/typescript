type SetCell = ([x, y]: [number, number], newValue: "X" | "O") => void;

export interface IPlayer {
  _name: string;
  _shape: "X" | "O";
  set name(n: string);
  get name(): string;
  set shape(n: "X" | "O");
  get shape(): "X" | "O";
}

export interface IBoard {
  _board: string[][];
  findShapeWithStraightLine: () => string | null;
  setCell: SetCell;
  isFull: () => boolean;
  isCellFull: (cell: [number, number]) => boolean;
  isCellValid: (cell: [number, number]) => boolean;
  draw: () => void;
}
