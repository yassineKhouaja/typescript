import { IBoard } from "./interfaces";

export class Board implements IBoard {
  _board: string[][];
  constructor() {
    this._board = Board.makeEmptyBoard();
  }
  static makeEmptyBoard = () => [
    ["", "", ""],
    ["", "", ""],
    ["", "", ""],
  ];

  isFull = () => !this._board.some((r) => r.some((c) => c === ""));
  findShapeWithStraightLine = () => {
    if (
      this._board[0][0] &&
      ((this._board[0][0] === this._board[0][1] && this._board[0][1] === this._board[0][2]) ||
        (this._board[0][0] === this._board[1][0] && this._board[1][0] === this._board[2][0]) ||
        (this._board[0][0] === this._board[1][1] && this._board[1][1] === this._board[2][2]))
    )
      return this._board[0][0];

    if (
      this._board[1][1] &&
      ((this._board[0][1] === this._board[1][1] && this._board[1][1] === this._board[2][1]) ||
        (this._board[1][0] === this._board[1][1] && this._board[1][1] === this._board[1][2]))
    )
      return this._board[1][1];
    if (
      this._board[2][2] &&
      ((this._board[0][2] === this._board[1][2] && this._board[1][2] === this._board[2][2]) ||
        (this._board[2][0] === this._board[2][1] && this._board[2][1] === this._board[2][2]))
    )
      return this._board[2][2];

    return null;
  };

  setCell = ([x, y]: number[], newValue: "X" | "O") => (this._board[x][y] = newValue);

  isCellFull = ([x, y]: number[]) => this._board[x][y] !== "";

  isCellValid = (cell: number[]) =>
    cell.every((v) => /^[0-2]$/.test(String(v))) && !this.isCellFull(cell);
  draw = () => {
    console.log("\n");
    this._board.forEach((r) => console.log(r.map((c) => c || "-").join(" ")));
    console.log("");
  };
}
