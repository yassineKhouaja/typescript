"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Game = void 0;
const Player_1 = require("./Player");
const Board_1 = require("./Board");
const prompt = require("prompt");
class Game {
    constructor() {
        this._board = new Board_1.Board();
        this._players = [];
        this._playerToPlayNextIdx = 0;
        this._turns = 0;
        this.gameStatus = Game.GAME_STATUES.IS_NOT_OVER;
    }
    askForNameAndShape() {
        return __awaiter(this, void 0, void 0, function* () {
            return prompt.get(Game.ASK_FOR_NAME_PROPS);
        });
    }
    createPlayer(message) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log("\n" + message);
            let player = null;
            do {
                try {
                    const { name, shape } = yield this.askForNameAndShape();
                    player = new Player_1.Player(name, shape);
                }
                catch (err) {
                    console.log(err.message);
                }
            } while (!player);
            return player;
        });
    }
    createPlayers() {
        return __awaiter(this, void 0, void 0, function* () {
            this._players[0] = yield this.createPlayer("Player1");
            this._players[1] = yield this.createPlayer("Player2");
        });
    }
    askPlayerForCell() {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield prompt.get(Game.ASK_FOR_CELL_PROPS.map((prop) => (Object.assign(Object.assign({}, prop), { conform: (input) => this._board.isCellValid(input.split(",").map((v) => parseInt(v))) }))));
            return result.cell.split(",").map((v) => parseInt(v));
        });
    }
    iterateGame() {
        return __awaiter(this, void 0, void 0, function* () {
            while (this.gameStatus === Game.GAME_STATUES.IS_NOT_OVER) {
                const player = this._players[this._playerToPlayNextIdx];
                console.log(`\n**Turn ${this._turns + 1}`);
                console.log(`*Player ${player.name}`);
                const cell = yield this.askPlayerForCell();
                this._board.setCell(cell, player.shape);
                this._board.draw();
                this._playerToPlayNextIdx = this._playerToPlayNextIdx === 0 ? 1 : 0;
                this.updateGameStatus();
            }
        });
    }
    updateGameStatus() {
        const shapeWithStraightLine = this._board.findShapeWithStraightLine();
        if (shapeWithStraightLine) {
            const playerIdx = this._players.findIndex(({ shape }) => shape === shapeWithStraightLine);
            this.gameStatus = Game.GAME_STATUES.PLAYER_1_WINNER;
        }
        if (this._board.isFull())
            return (this.gameStatus = Game.GAME_STATUES.TIE);
        return (this.gameStatus = Game.GAME_STATUES.IS_NOT_OVER);
    }
    printResults() {
        switch (this.gameStatus) {
            case Game.GAME_STATUES.PLAYER_1_WINNER:
                return console.log(`${this._players[0].name} is Winner !`);
            case Game.GAME_STATUES.PLAYER_2_WINNER:
                return console.log(`${this._players[1].name} is Winner !`);
            case Game.GAME_STATUES.TIE:
                return console.log(`No Winner! Game is a tie`);
            default:
                return console.log("Something went wrong");
        }
    }
    start() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.createPlayers();
            yield this.iterateGame();
            this.printResults();
            // promopt player1 and 2 alternatively for choices
            // check winner every time
            // endGame
        });
    }
}
exports.Game = Game;
Game.GAME_STATUES = {
    PLAYER_1_WINNER: 1,
    PLAYER_2_WINNER: 2,
    TIE: 0,
    IS_NOT_OVER: -1,
};
Game.ASK_FOR_NAME_PROPS = [
    {
        name: "name",
        // validator: /^[a-zA-z\s]{2,}$/,
        warning: "Name must be only letters or spaces, and at least two characters long",
        conform: Player_1.Player.isName,
    },
    {
        name: "shape",
        conform: Player_1.Player.isShape,
        warning: 'Shape must be either "X" or "O"',
    },
];
Game.ASK_FOR_CELL_PROPS = [
    { name: "cell", description: 'Enter cell in format "x,y"', warning: "Invalid choice" },
];
