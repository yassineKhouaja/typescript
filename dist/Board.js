"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Board = void 0;
class Board {
    constructor() {
        this.isFull = () => !this._board.some((r) => r.some((c) => c === ""));
        this.findShapeWithStraightLine = () => {
            if (this._board[0][0] &&
                ((this._board[0][0] === this._board[0][1] && this._board[0][1] === this._board[0][2]) ||
                    (this._board[0][0] === this._board[1][0] && this._board[1][0] === this._board[2][0]) ||
                    (this._board[0][0] === this._board[1][1] && this._board[1][1] === this._board[2][2])))
                return this._board[0][0];
            if (this._board[1][1] &&
                ((this._board[0][1] === this._board[1][1] && this._board[1][1] === this._board[2][1]) ||
                    (this._board[1][0] === this._board[1][1] && this._board[1][1] === this._board[1][2])))
                return this._board[1][1];
            if (this._board[2][2] &&
                ((this._board[0][2] === this._board[1][2] && this._board[1][2] === this._board[2][2]) ||
                    (this._board[2][0] === this._board[2][1] && this._board[2][1] === this._board[2][2])))
                return this._board[2][2];
            return null;
        };
        this.setCell = ([x, y], newValue) => (this._board[x][y] = newValue);
        this.isCellFull = ([x, y]) => this._board[x][y] !== "";
        this.isCellValid = (cell) => cell.every((v) => /^[0-2]$/.test(String(v))) && !this.isCellFull(cell);
        this.draw = () => {
            console.log("\n");
            this._board.forEach((r) => console.log(r.map((c) => c || "-").join(" ")));
            console.log("");
        };
        this._board = Board.makeEmptyBoard();
    }
}
exports.Board = Board;
Board.makeEmptyBoard = () => [
    ["", "", ""],
    ["", "", ""],
    ["", "", ""],
];
